# This repository moved #
Please use the new location: https://github.com/mintforpeople/robobo-sensing

# Robobo Sensing Modules

This repository contains different modules that provide a variety of sensing capabilities to the robot, from the accelerometer and giroscope of the smartphone (OBO) to the battery of the smartphone.
